class ArticlesController < ApplicationController
	before_action :check_user, only: [:edit]
	before_action :set_article , only: [:show ,:edit ,:update ,:destroy]
	before_action :authenticate_user!
	def show
		
	end

	def check_user
		@article=Article.friendly.find(params[:id])
		unless current_user.id == @article.user_id
			flash[:alert]="You are not authorized perform this action"
			redirect_to (request.referrer || articles_path)
			return
		end
	end

	def index
		@articles=Article.all()		
	end
	def new
		@article=Article.new
	end
	def create
		@article=Article.new(set_article_params)
		@article.user_id = current_user.id
		if @article.save
			flash[:notice]="Article sumbitted successfully"
			redirect_to article_path(@article)

		else
			render 'new'
		end
	end
	def edit
		
	end
	def update
		if @article.update(set_article_params)
			flash[:notice]="Article updated successfully"
			redirect_to article_path(@article)
		else
			render 'edit'
		end
		
	end
	def destroy
		
		@article.destroy
		redirect_to @article
		
	end
	private
	def set_article
		@article=Article.friendly.find(params[:id])

	end
	def set_article_params
		params.require(:article).permit(:title,:description)

	end
	
end