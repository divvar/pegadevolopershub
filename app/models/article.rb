class Article < ApplicationRecord
	extend FriendlyId
	friendly_id :title, use: :slugged
	belongs_to :user
	validates :title, presence: true,length: {minimum: 6,maximum:100}
	validates :description, presence: true, length: {minimum: 6}

end