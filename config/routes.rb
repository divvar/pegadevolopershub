Rails.application.routes.draw do
  devise_for :admins
	devise_for :users ,path: '',path_names: {sign_in:'login',sign_out:'logout', sign_up:'register'}
	#get 'show', to: 'pages#show'
	authenticated :user do
		root to: 'articles#index', as: :authenticated_root
	end
	root 'pages#home'
	resources :articles 


end
